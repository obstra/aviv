import React from 'react';
import "tui-image-editor/dist/tui-image-editor.css";
import "./App.css"
import ImageEditor from "@toast-ui/react-image-editor";
require("file-saver");

function App() {
  return (
    <>
    <div className="main" >מחולל חתימות</div>
        <ImageEditor
          includeUI={{
            loadImage: {
              path: "signature.jpg",
              name: "signature"
            },
            locale: {
              Crop: "חיתוך",
              Flip: "הפוך",
              Draw: "צייר",
              Rotate: "סובב",
              Text: "מלל",
              Mask: "תמונה",
              Delete: "מחק",
              Reset: "אפס",
              Redo: "בטל",
              Undo: "חזור",
              DeleteAll: "מחק הכול"
            },
            menu: ["crop", "flip", "rotate", "draw", "text", "mask"],
            initMenu: "filter",
            uiSize: {
              width: "100%",
              height: "100vh"
            },
            menuBarPosition: "bottom"
          }}
          selectionStyle={{
            cornerSize: 20,
            rotatingPointOffset: 70
          }}
          usageStatistics={false}
        />
          </>
  );
}

export default App;
